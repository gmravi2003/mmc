function[gfit]=Interpolate(prod,observed_indices,...
    xhat_qp,zhat_qp,in_arg)
%zhat_qp is the set of input knots.
%xhat_qp is the set of output knots.
% I need the outputs at all the input points in prod


[n1,n2]=size(prod);
gfit=nan(n1,n2);

% since we are using pav_interpolate_sorted, hence we shal have to 
% sort before hand
[zhat_qp_sorted, ind_sort] = sort(zhat_qp(:));
temp=xhat_qp(:);
xhat_qp_sorted = temp(ind_sort);

%in_arg is either full or limited. In the case of limited, interpolation
% is done only at those points where observations have been made

if(strcmpi(in_arg,'limited'))
    for i=1:n1
        for j =1:n2
            if(observed_indices(i,j))                
                gfit(i,j)=...
                pav_interpolate_sorted(prod(i,j),zhat_qp_sorted,...
                xhat_qp_sorted);
            end
        end
    end
else
    for i=1:n1
        gfit(i,:)=...
            pav_interpolate_sorted(prod(i:i,:),zhat_qp_sorted,...
            xhat_qp_sorted);
    end
end