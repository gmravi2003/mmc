%%%%%%%%%%%%%% CALCULATIONS BEGIN %%%%%%%%%%%%%
function[Xest,rmse,proj_dim]=mmc_nofac(Xtrn,Xval,lip_const,step_size,proj_dim,...
                val_indices,maxiter,num_grad_steps,maxrank,incr_rank,...
                incr_rank_flag,decr_step_flag,svd_method,lpav_method,...
                F_mat,rho,tol_mmc,calibrated_flag)
Zest=Xtrn;
observed_indices=~isnan(Xtrn);
Zest(~observed_indices)=0;
omega_size=sum(observed_indices(:));

[m,n]=size(Xtrn);
size_X=m*n;
Zest=Zest*size_X/omega_size;
zknots=Zest(observed_indices);
xknots=Xtrn(observed_indices);
xhat_pav=xknots;
rmse_vec=Inf(maxiter,1);
for iter1=1:maxiter
    
    % Update Z using gradient steps and hard thresholding. Note that
    % these gtradient steps use calibrated loss function.
    
    
    [Zest]=IHTZ(Zest,xknots,zknots,xhat_pav,observed_indices,...
        step_size,num_grad_steps,proj_dim,svd_method,calibrated_flag);
    
    
    
    % Update function via an LPAV fit
    zknots=Zest(observed_indices);
    
    if(strcmpi(lpav_method,'admm'))
        xhat_pav_new=lpav_admm(zknots,xknots,rho,F_mat);
    else
        % Just call MOSEK.
        xhat_pav_new=lpav(zknots,xknots,lip_const);
    end
    % With this we are done with one round of learning function
    % and learning Z matrix. 
    
    % Check to see if rank needs to be increased
    frac=...
        norm(xhat_pav_new-Xtrn(observed_indices))/...
        norm(xhat_pav-Xtrn(observed_indices));
    
    reschg=abs(1-frac);
    if(reschg<10*tol_mmc && incr_rank_flag&& proj_dim<maxrank)
        % Increase rank
        proj_dim=proj_dim+incr_rank;
        proj_dim=min(proj_dim,maxrank);
        %fprintf('Increased rank to %d...\n',proj_dim);
    end
    % update lattice points. We already have correct zknots.
    xhat_pav=xhat_pav_new;
    
    err=norm(xhat_pav-Xtrn(observed_indices));
    rmse=err/sqrt(length(err));
    rmse_vec(iter1)=rmse;
    % Calculate err
    relres=err/norm(Xtrn(observed_indices));
    %fprintf('iter:%d, rankest:%d, relres:%f...\n',...
        %iter1,proj_dim,relres);
    %if(relres<tol_mmc)
     %  break;
    %end
    if(decr_step_flag)
        step_size=step_size*sqrt(iter1)/sqrt(iter1+1);
    end
end
Xest=Interpolate(Zest,observed_indices,xhat_pav,zknots,'full');
err=Xest(val_indices)-Xval(val_indices);
rmse=norm(err)/sqrt(length(err));
rmse_vec(iter1+1:end)=rmse_vec(iter1);
%dlmwrite('rmse_vec_c_40_p_0.5.txt',rmse_vec);

%{

% Defaults for this blog post
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 14;      % Fontsize
lw = 5.5;      % LineWidth
msz = 8;       % MarkerSize

figure(2);
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties
plot(rmse_vec,'LineWidth',lw,'MarkerSize',msz); %<- Specify plot properites
xlabel('Number of iterations','FontWeight','bold');
ylabel('RMSE on train set','FontWeight','bold');
%title('RMSE of MMC-c on the training set with number of iterations','FontWeight','bold');

set(gcf,'InvertHardcopy','on');
set(gcf,'PaperUnits', 'inches');
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);

% Save the file as EPS
print('rmse_iters','-dpng','-r300');

system('gs -o -q -sDEVICE=png256 -dEPSCrop -r300 -ormse_iters.png rmse_iters.eps');
%}
end



