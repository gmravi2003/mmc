% LPAV calls MOSEK which is an external package for solving 
% a Quadratic program.

function[xhat]=lpav(zknots,xknots,lip_const)

% LPAV implementation of Lipschitz monotonic regression
[sorted_zknots,sorted_order]=sort(zknots,'ascend');
n=length(zknots);
A=sparse(zeros(n-1,n));


%%%% This piece enforces monotonicity %%%%%%%%%%%%
for i=1:n-1
    A(i,sorted_order(i))=1;
    A(i,sorted_order(i+1))=-1;
end


%%%%%%% This piece of code enforces Lipschitz constraints %%%%%%%%%
A1=sparse(zeros(n-1,n));
for i=1:n-1
    A1(i,sorted_order(i))=-1;
    A1(i,sorted_order(i+1))=1;
end
b1=lip_const*diff(sorted_zknots);
% Concatenate A and b1
A=[A;A1];
b=[zeros(n-1,1);b1];
v=-2*xknots;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
options=optimset('Display','off','Algorithm','interior-point-convex');
Aeq=[];beq=[];lb=[];ub=[];
H=2*speye(n);
x0=mean(xknots)*ones(n,1);
%x0=zeros(n,1);
[xhat,~,exitflag,output]=quadprog(H,v,A,b,Aeq,beq,lb,ub,x0,options);
%}
%if exitflag<1
 %   display(exitflag);
 %   display(output);
%end
% If we are using mosek

clear prob;
prob.c=v;
prob.qosubi=1:n;
prob.qosubj=1:n;
prob.qoval=2*ones(n,1);


prob.a=sparse(A);
prob.blc=[];
prob.buc=b;
prob.blx=-inf*ones(n,1);
%[res] = mskqpopt(H,v,A,[],b,[],[]); 
[r,res]=mosekopt('minimize echo(0)',prob);

xhat=res.sol.itr.xx;
%display(minmax(xhat'));
%display(res.sol.itr.solsta);

end
