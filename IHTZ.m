% Iterative hard thresholding routine for lifted
% matrix completion
function[Zt]=IHTZ(Z0,xknots,zknots,xhat_pav,...
    observed_indices,step_size,num_grad_steps,...
    proj_rank,svd_method,calibrated_flag)

% Perform k gradient steps, each time projection onto 
% the cone of low-rank matrices of rank proj_rank


Zt=Z0;
zt=Z0(observed_indices);
if(~ calibrated_flag)
    [zt_sorted, ind_sort] = sort(zt,'ascend');
    temp=xhat_pav(:);
    xhat_pav_sorted = temp(ind_sort);
    delta_x=diff(xhat_pav_sorted);
    if(length(find(delta_x<0))>0)
        display(['Minimum was=',num2str(min(delta_x))]);
    end
    delta_z=diff(zt_sorted);
    slope_vec=delta_x./delta_z;
    slope_vec(isnan(slope_vec))=0;
    slope_vec=[slope_vec;0];
    slope_vec=slope_vec(ind_sort);
end
for t=1:num_grad_steps
    if(t>1)
        res=Interpolate(Zt,observed_indices,...
            xhat_pav,zknots,'limited');
    else
        % for the first step res is known
        res=nan(size(Zt));
        res(observed_indices)=xhat_pav;
    end
    xhat=res(observed_indices);
    %if(sum(isnan(xhat(:)))>0)
     %fprintf('There are nans in xhat here..\n');
    %end
    
    if(~ calibrated_flag)
        % This means we are not using calibrated loss
        % put it back into matrix form
        
        % Here the updates are as follows
        % Z <- Z- \eta(g(Z) - X) g'(Z)
        % We shall replace g' with L
        
        zt=zt-(step_size*(xhat-xknots).*slope_vec);
        
    else
        % We are running calibrated updates.
        zt=zt-step_size*(xhat-xknots);
    end
    Zt(observed_indices)=zt;
    
    Zt=ProjectCone(Zt,proj_rank,svd_method);
    zt=Zt(observed_indices);
end
end