clc;clear all;

%%%% If you are using synthetic datasets%%%
% Available choices
%c_vec \in[1,10,40];
%p_vec \in [0.7,0.5,0.35,0.2];


c=40;
p=0.5;


dataname=['c_',num2str(c),'_p_',num2str(p)];
[trndatafile,tstdatafile,...
    valdatafile,maxrank]=GetFileNames(dataname);
display(trndatafile);
display(tstdatafile);
display(valdatafile);

Xtrn=dlmread(trndatafile);
Xtst=dlmread(tstdatafile);
Xval=dlmread(valdatafile);
%fprintf('Finished reading all data files...\n');
val_indices=find(~isnan(Xval));
tst_indices=find(~isnan(Xtst));



%%%%%%%%% PARAMETER SETUP FOR MONOTONIC MATRIX COMPLETION (MMC) %%%%%%%%%%%%%%


svd_method='propack';
lpav_method='mosek';

% If calibrated flag is set to false then will run squared loss based algorithm
calibrated_flag=true; 

% Number of gradient steps used to update Z matrix. All our algorithms are 
% designed to use just 1 step.
num_grad_steps=1; 

% Tolerance parameter. Usually 10^-2 or 10^-3 is enough.
tol_mmc=10^-2;

% Maximum Number of iterations. 50-100 iterations are enough
maxiter=50;

% Step size used for gradient calculations. If a range is provided then
% we shall perform validation over the entire range and pick the best one
step_size_vec=[1:2:20];

% Initial rank estimate. For MMC-1 feel free to use a larger number
proj_dim=2; 

% For MMC-c and MMC-LS the rank estimate is determined adaptively.
% incr_rank tells by how much should we increase our estimate of rank. A
% small number is preferred.
incr_rank=1;

% This is the maximum estimate of rank.
maxrank=ceil(3*maxrank/4);

% This flag tells if we want to use the increasing rank strategy to
% adaptively determine the rank
incr_rank_flag=true;

% In gradient descent a common strategy to use is decreasing step size. 
% We found that the best results were obtained when the step size used is a
% constant. Set the flag to false to use a constant step size
decr_step_flag=false;

% The lipschitz constant of the link function. For synthetic experiments
% set this to c/4. For our experiments with real dataset this was set to a
% small number (<=10)
lip_const=c/4;


%{
if(strcmpi(svd_method,'propack'))
    fprintf('Using PROPACK for SVD calculations...\n');
end
if(strcmpi(svd_method,'svds'))
    fprintf('Using SVDS for SVD calculations...\n');
end
if(strcmpi(svd_method,'randomized_svd'))
    fprintf('Using RANDOMIZED SVD for SVD calculations...\n');
end

if(strcmpi(lpav_method,'admm'))
    fprintf('Using ADMM for LPAV calculations...\n');
else
    fprintf('Using MOSEK for LPAV calculations..\n');
end
%}



% If we are running expts on opt machine then addpath
[~,val]=unix('hostname');
if (~isempty(strfind(val,'opt')))
    % this is opt machine
    fprintf('Added path to mosek...\n');
    addpath '/data/gmravi/mosek/7/toolbox/r2013aom';
    addpath(genpath('/data/gmravi/matlab_codes/utils/PROPACK'));
else
    addpath(genpath('/Users/guestaccount/mosek/7/toolbox/r2013aom'));
    addpath(genpath('/Users/guestaccount/matlab_codes/utils/PROPACK'));
end

min_rmse=inf;

if(strcmp(lpav_method,'admm'))
    % We need an F_mat
    display('!!!!!! WARNING: LIPSCHITZ CONSTANT HAS NOT BEEN TAKEN INTO ACCOUNT');
    display('!!!! REWRITE CODE TO MAKE USE OF LIPSCHITZ CONSTANT');
    rho=20;
    observed_indices=~isnan(Xtrn);

    n=sum(observed_indices(:));
    
    k=2*n-2;
    P_plus_rho=[(2+rho)*speye(n),sparse(n,k)];
    mat=[sparse(k,n),rho*speye(k)];
    P_plus_rho=[P_plus_rho;mat];


    index_vec_i=zeros(k,1);
    index_vec_i(1:2:end)=[1:n-1];
    index_vec_i(2:2:end)=[1:n-1];

    index_vec_j=zeros(k,1);
    index_vec_j(1:2:end)=[1:n-1];
    index_vec_j(2:2:end)=[2:n];

    index_vec_i=[index_vec_i;n-1+index_vec_i];
    index_vec_j=[index_vec_j;index_vec_j];

    vals1=ones(k,1);
    vals1(2:2:end)=-1;

    vals2=-1*ones(k,1);
    vals2(2:2:end)=1;
    vals=[vals1;vals2];

    M=sparse(index_vec_i,index_vec_j,vals);
    M_bar=[M,speye(k)];
    F_mat=[[P_plus_rho,M_bar'];[M_bar,sparse(k,k)]];
else
    rho=inf;
    F_mat=[];
end
for j=1:length(step_size_vec)
    
    val_flag=true;
    step_size=step_size_vec(j);


    [Xest,rmse,est_dim]=mmc_nofac(Xtrn,Xval,lip_const,step_size,...
        proj_dim,val_indices,maxiter,num_grad_steps,maxrank,...
        incr_rank,incr_rank_flag,decr_step_flag,svd_method,...
        lpav_method,F_mat,rho,tol_mmc,calibrated_flag);

    if(rmse<min_rmse)
        min_rmse=rmse;
        Xest_best=Xest;
        opt_step_size=step_size;
        opt_est_dim=est_dim;
        %fprintf('Found better settings:err=%f..\n',rmse);
    end
    %fprintf('Finished j=%d/%d,err=%f, est_opt_dim=%f...\n',...
     %   j,length(step_size_vec),rmse,opt_est_dim);
end
err=Xest_best(tst_indices)-Xtst(tst_indices);
rmse_opt=norm(err)/sqrt(length(err));
%fprintf('Number of test entries=%d...\n',length(err));
fprintf('Opt step=%f,tst_err=%f,dim=%f\n',opt_step_size,rmse_opt,opt_est_dim);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
mu=0.0001;
[trn_indices]=find(~isnan(Xtrn));
observations=Xtrn(trn_indices);
[n1,n2]=size(Xtrn);

Xmatcomp=solver_sNuclearBP( {n1,n2,trn_indices}, observations, mu);
err=Xmatcomp(tst_indices)-Xtst(tst_indices);
err_per_element_matcomp=sqrt(norm(err)^2/numel(err));
fprintf(['c=',num2str(c),' p=',num2str(p)]);
fprintf(' RMSE=%f...\n',err_per_element_matcomp);
rmse_opt=err_per_element_matcomp;
%}