clc;clear;
Ztrue=dlmread('DATA/synthetic_new/lowrank_matrix.txt'); % This is the original low rank matrix
c=20;
Xtrue=1./(1+exp(-c*Ztrue));
[U,S,V]=svd(Xtrue,'econ');
S=diag(S);
S=S.^2;
cs=cumsum(S);
tot_sum=sum(S);
frac=cs/tot_sum;
eps=0.01;
res=1-eps^2;
indx=find(frac>res,1);
display(indx);