function[trndatafile,tstdatafile,valdatafile,maxrank]=...
    GetFileNames(dataname)

if(strcmpi(dataname,'jester_small'))
    trndatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.trn.csv';
    tstdatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.tst.csv';
    valdatafile='../lifted_mc/DATA/jester/jester-data-3.small.proc.val.csv'; 
    maxrank=65;
end

if(strcmpi(dataname,'jester'))
    trndatafile='../lifted_mc/DATA/jester/jester-data-3.proc.trn.csv';
    tstdatafile='../lifted_mc/DATA/jester/jester-data-3.proc.tst.csv';
    valdatafile='../lifted_mc/DATA/jester/jester-data-3.proc.val.csv'; 
    maxrank=66;
end
if(strcmpi(dataname,'ml-100k'))
    
    % for ml-100k we shall simply use u1 data files for train & val
    % u2 files for train &test
    trndatafile='../lifted_mc/DATA/ml-100k/u1.base.proc.txt';
    valdatafile='../lifted_mc/DATA/ml-100k/u1.val.proc.txt'; 
    tstdatafile='../lifted_mc/DATA/ml-100k/u1.test.proc.txt';
    maxrank=391;
end

if(strcmpi(dataname,'paper_reco_small'))
    trndatafile='DATA/paper_reco/paper_reco_small_train.txt';
    tstdatafile='DATA/paper_reco/paper_reco_small_test.txt';
    valdatafile='DATA/paper_reco/paper_reco_small_val.txt';
    %maxrank=26;
    maxrank=28;
end
if(strcmpi(dataname,'paper_reco_large'))
    trndatafile='DATA/paper_reco/paper_reco_large_train.txt';
    tstdatafile='DATA/paper_reco/paper_reco_large_test.txt';
    valdatafile='DATA/paper_reco/paper_reco_large_val.txt';
    maxrank=47;
end
if(strcmpi(dataname,'barbara'))
    trndatafile='../lifted_mc/DATA/images/barbara.trn';
    tstdatafile='../lifted_mc/DATA/images/barbara.tst';
    valdatafile='../lifted_mc/DATA/images/barbara.val';
    maxrank=400;
end
if(strcmpi(dataname,'lenna'))
    trndatafile='../lifted_mc/DATA/images/lenna.trn';
    tstdatafile='../lifted_mc/DATA/images/lenna.tst';
    valdatafile='../lifted_mc/DATA/images/lenna.val';
    maxrank=406;
end
if(strcmpi(dataname,'clown'))
    trndatafile='../lifted_mc/DATA/images/clown.trn';
    tstdatafile='../lifted_mc/DATA/images/clown.tst';
    valdatafile='../lifted_mc/DATA/images/clown.val';
    maxrank=346;
end
if(strcmpi(dataname,'crowd'))
    trndatafile='../lifted_mc/DATA/images/crowd.trn';
    tstdatafile='../lifted_mc/DATA/images/crowd.tst';
    valdatafile='../lifted_mc/DATA/images/crowd.val';
    maxrank=391;
end
if(strcmpi(dataname,'girl'))
    trndatafile='../lifted_mc/DATA/images/girl.trn';
    tstdatafile='../lifted_mc/DATA/images/girl.tst';
    valdatafile='../lifted_mc/DATA/images/girl.val';
    maxrank=405;
end

if(strcmpi(dataname,'cameraman'))
    trndatafile='../lifted_mc/DATA/images/cameraman.trn';
    tstdatafile='../lifted_mc/DATA/images/cameraman.tst';
    valdatafile='../lifted_mc/DATA/images/cameraman.val';
    maxrank=393;
    
end

if(strfind(dataname,'c_'))
    
    trndatafile=['DATA/synthetic_new/xtrn_',dataname,'.txt'];
    tstdatafile=['DATA/synthetic_new/xtst_',dataname,'.txt'];
    valdatafile=['DATA/synthetic_new/xval_',dataname,'.txt'];
    maxrank=20;


end